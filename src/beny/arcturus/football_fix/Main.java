package beny.arcturus.football_fix;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadItemsManagerEvent;
import com.eu.habbo.habbohotel.items.ItemInteraction;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Beny
 */
public class Main extends HabboPlugin implements EventListener {

    @Override
    public void onEnable() {
        Emulator.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
    }

    @Override
    public boolean hasPermission(Habbo habbo, String string) {
        return false;
    }
    
    @EventHandler
    public void onEmulatorLoadItemsManagerEvent(EmulatorLoadItemsManagerEvent event) {
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection(); Statement statement = connection.createStatement()) {
            statement.execute("UPDATE `items_base` SET `interaction_type` = 'football_fixed' WHERE `interaction_type` = 'football' OR `item_name` LIKE 'fball_ball%';");
        } catch (SQLException ex) {
        }
        Emulator.getGameEnvironment().getItemManager().addItemInteraction(new ItemInteraction("football_fixed", InteractionFootball.class));
        Emulator.getLogging().logStart("Football fix by Beny. enabled");
    }
    
}
